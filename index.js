// console.log("Monday yay");

// An array is a list of data that is relate or connected to each other.

let studentNuberA = "2020-1923";
let studentNuberB = "2020-1924";
let studentNuberC = "2020-1925";
let studentNuberD = "2020-1926";
let studentNuberE = "2020-1927";

// Array 

let studentNumber = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

//  [Setion] Array
/*
	-Used to store multiple related valued in a variable;
	-Declared using square brackets ([]) also knows as "Array Literals".
	-Stores numerous amounts of data to manipulate
	-Provides access to a nuumber of functions/methods that help achive specific tasks.

 	Syntax:
 	let/const arrayName = [elementA, elementB, elementC ...]
*/

// common examples of an Array
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];

console.log(cities);
console.log(cities[2].length);

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"];

console.log(myTasks)
myTasks.length = myTasks.length-1;
// myTasks.length = myTasks.length--;
console.log(myTasks)

//  Looping over an array
	let newArr = [
		"Name",
		"Name2"
		];
	
	newArr[newArr.length] = "Name3";
	newArr[newArr.length] = "Name4";

	for(let index = 0; index < newArr.length; index++){
		console.log(newArr[index]);
	}	

	let numArr = [5, 12, 30 , 46, 30];

		for(let i = 0; i < numArr.length; i++){
			if(numArr[i] % 5 === 0){
				console.log(numArr[i] + " is divisible bt 5.");
			}else{
				console.log(numArr[i] + " is not divisible bt 5.");
			}
		}

	// Multidimensional Arrays

		let chessBoard = [];

		for(i = 0; i < 9; i++){
			chessBoard[chessBoard.length] = ["a" + i , "b" + i , "c" + i, "d" + i , "e" + i, "f" + i, "g" + i, "h" + i];
		}

		console.table(chessBoard);

		// ["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"],
		// 	["a1" , "b1" , "c1", "d1" , "e1", "f1", "g1", "h1"]